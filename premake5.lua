workspace "SimpleRayTrace"
    architecture "x64"
    startproject "SimpleRayTrace"
    configurations
	{
		"Debug",
		"Release"
	}
	outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

	project "SimpleRayTrace"
		location "SimpleRayTrace"
		kind "ConsoleApp"
		language "C++"
		cppdialect "C++17"
		staticruntime "on"

		targetdir 	("bin/" 	.. outputdir .. "/%{prj.name}")
		objdir 		("bin/int/" .. outputdir .. "/%{prj.name}")

		files 
		{
			"%{prj.name}/**.h",
			"%{prj.name}/**.cpp",
			"%{prj.name}/**.c"
		}
		
		includedirs
		{

		}

		filter "system:windows"
			systemversion "latest"
		
		filter "configurations:Debug"
			defines "RT_DEBUG"
			runtime "Debug"
			symbols "on"

		filter "configurations:Release"
			defines "RT_RELEASE"
			runtime "Release"
			optimize "on"