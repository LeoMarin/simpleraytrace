#pragma once

#include "Ray.h"
#include "Random.h"

class Material
{
public:
	Material() {};
	virtual bool Scatter(const Ray& ray, Vec3f& scatterDirection) const = 0;
	virtual Vec3f Albedo() const = 0;
};

// material that scatters light
class Lambertian : public Material
{
public:
	Lambertian(const Vec3f& albedo) :m_Albedo(albedo) {}

	virtual bool Scatter(const Ray& ray, Vec3f& scatterDirection) const
	{
		scatterDirection = ray.HitNormal() + RandomPointInUnitSphere();
		return true;
	}

	virtual Vec3f Albedo() const
	{
		return m_Albedo;
	}
private:
	Vec3f m_Albedo;
};

// material that reflects light
class Metalic : public Material
{
public:
	Metalic(const Vec3f& albedo, float fuzzines) :m_Albedo(albedo), m_Fuzzines(fuzzines) {}

	virtual bool Scatter(const Ray& ray, Vec3f& scatterDirection) const
	{
		Vec3f reflectedDirection = Vec3f::Reflect(ray.Direction(), ray.HitNormal());
		scatterDirection = reflectedDirection + RandomPointInUnitSphere() * m_Fuzzines;

		//return true;
		return scatterDirection.dot(ray.HitNormal()) > 0;
	}

	virtual Vec3f Albedo() const
	{
		return m_Albedo;
	}
private:
	Vec3f m_Albedo;
	float m_Fuzzines;
};

// material that refracts light
class Dielectric : public Material
{
public:
	Dielectric(const Vec3f& albedo, float refractionIndex) :m_Albedo(albedo), m_RefractionIndex(refractionIndex) {}

	virtual bool Scatter(const Ray& ray, Vec3f& scatterDirection) const
	{
		Vec3f otwardNormal;
		Vec3f reflectedDirection = Vec3f::Reflect(ray.Direction(), ray.HitNormal());
		Vec3f refractedDirection;
		
		// n2/n1
		float refractionQuotient;

		// cosine used in Shlick aproximation
		float cosine;
		float reflectionProbability;

		// ray is coming from outside of the object
		if (ray.Direction().dot(ray.HitNormal()) > 0)
		{
			otwardNormal = -ray.HitNormal();
			// n2/n1 = refractionIndex/1
			refractionQuotient = m_RefractionIndex;

			cosine = ray.Direction().dot(ray.HitNormal()) * m_RefractionIndex;
		}
		// ray is coming from inside the object
		else
		{
			otwardNormal = ray.HitNormal();
			// n2/n1 = 1/refractionIndex
			refractionQuotient = 1 / m_RefractionIndex;

			cosine = -ray.Direction().dot(ray.HitNormal());
		}

		// check if ray will refract
		if (Refract(ray, otwardNormal, refractionQuotient, refractedDirection))
		{
			// if ray is refracted calculate Schlick's Approximation
			reflectionProbability = SchlickApproximation(m_RefractionIndex, cosine);
		}
		else
		{
			// if ray is not refracted, create scatter ray from reflection
			scatterDirection = reflectedDirection;
			return true;
		}

		// generate random number to determine if ray is reflected or reftacted
		if ((rand() % 100) / (float)100 < reflectionProbability)
		{
			// ray is reflected
			scatterDirection = reflectedDirection;
		}
		else
		{
			// ray is refracted
			scatterDirection = refractedDirection;
		}
		return true;
	}

	virtual Vec3f Albedo() const
	{
		return m_Albedo;
	}

private:
	bool Refract(const Ray& ray, const Vec3f& normal, float refractionQuotient, Vec3f& refractedDirection) const
	{
		// n1 sin(fi1) = n2 sin (fi2)
		// sin(fi2) = n2/n1 sqrt(1 - cos2(fi1))
		// cos(fi2) = sqrt(1 - sin2(fi2))
		// cos(fi2) = sqrt(1 - (n2/n1)2 (1 - cos2(fi1))
		float cosFi1 = ray.Direction().dot(normal);
		float discriminant = 1 - refractionQuotient * refractionQuotient * (1 - cosFi1 * cosFi1);
		if (discriminant > 0)
		{
			refractedDirection = (ray.Direction() - normal * cosFi1) * refractionQuotient - normal * sqrt(discriminant);
			return true;
		}
		else
		{
			return false;
		}
	}

	// approximates contribution of reflection when light hits refracting surface
	float SchlickApproximation(float refractionIndex, float cosineFi) const
	{
		// R = R0 + (1 - R0)(1 - cosFi)^5
		// R0 = ((n1 - n2) / (n1 + n2))^2
		float r0 = (1 - refractionIndex) / (1 + refractionIndex);
		r0 = r0 * r0;
		return r0 + (1 - r0) * pow((1 - cosineFi), 5);
	}


private:
	Vec3f m_Albedo;
	float m_RefractionIndex;
};