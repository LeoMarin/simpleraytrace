#pragma once

#include <random>

#include "Vector.h"
#include "Ray.h"
#include "Material.h"

class Sphere
{
public:
	// constructors
	Sphere();
	Sphere(Vec3f center, float radius, Material* material);

	// destructor
	~Sphere() {}

	// access methods
	Vec3f Center() const;
	float Radius() const;

	bool Hit(Ray& ray) const;

	// finds point on sphere intersected by ray
	Vec3f PointOfIntersection(const Ray& ray) const;

	// check if sphere is intersected by ray
	bool IsIntersectedByRay(const Ray& ray) const;

	// finds surface normal for point on the sphere
	Vec3f NormalInPoint(const Vec3f& point) const;


	Material* MaterialPointer();

private:
	// sphere center defined by vector
	Vec3f m_Center;
	// sphere radius defined by float
	float m_Radius;
	// sphere material
	Material* m_Material;
};

inline Sphere::Sphere()
	:m_Center(Vec3f()), m_Radius(0), m_Material(nullptr)
{
}

Sphere::Sphere(Vec3f center, float radius, Material* material)
	:m_Center(center), m_Radius(radius), m_Material(material)
{
	// sphere constructor
}

inline Vec3f Sphere::Center() const
{
	return m_Center;
}

inline float Sphere::Radius() const
{
	return m_Radius;
}

inline bool Sphere::Hit(Ray& ray) const
{
	Vec3f originToCenter = ray.Origin() - m_Center;
	// calculate root for quadratic equation
	float a = ray.Direction().LengthSquared();
	float b = Vec3f::dot(originToCenter, ray.Direction());
	float c = originToCenter.LengthSquared() - m_Radius * m_Radius;
	float discriminant = b * b - a * c;

	// if disc < 0 no solution for quadratic equation
	// meaning no collision of ray and sphere
	if (discriminant > 0)
	{
		float root = sqrt(discriminant);

		// check first intersection of sphere and ray
		float temp = (-b - root) / a;
		if (temp < ray.TMax() && temp > ray.TMin())
		{
			// if this is the closest object hit so far 
			// record important values
			ray.SetTMax(temp);
			ray.SetHitPoint(ray.PointOnRay(temp));
			ray.SetNormalInHitPoint(NormalInPoint(ray.HitPoint()));
			ray.SetHitAlbedo(m_Material->Albedo());
			ray.SetMaterialPointer(m_Material);
			return true;
		}

		// check second intersection of sphere and ray
		temp = (-b + root) / a;
		if (temp < ray.TMax() && temp > ray.TMin())
		{
			ray.SetTMax(temp);
			ray.SetHitPoint(ray.PointOnRay(temp));
			ray.SetNormalInHitPoint(NormalInPoint(ray.HitPoint()));
			ray.SetHitAlbedo(m_Material->Albedo());
			ray.SetMaterialPointer(m_Material);
			return true;
		}
	}
	return false;
}

inline Vec3f Sphere::NormalInPoint(const Vec3f& point) const
{
	// v = p - center
	// vector v has direction of sphere normal in point p
	return (point - m_Center) / m_Radius;
}

inline Material* Sphere::MaterialPointer()
{
	return m_Material;
}
