#pragma once

#include "Vector.h"
#include "Ray.h"
#include "Random.h"

class Camera
{
public:
	// constructors
	Camera(const Vec3f& lookFrom, const Vec3f& lookAt, const Vec3f& upVector, float fov, float aspectRatio, float aperture, float focusDistance);

	// destructor
	~Camera() {}

	Ray GetRay(float u, float v) const;
private:
	// camera defined in center of coordinate system
	Vec3f m_Origin;
	// bottom left corner of image
	Vec3f m_BottomLeftCorner;
	
	Vec3f m_HorizontalStep;
	Vec3f m_VerticalStep;

	float m_LensRadius;
	Vec3f u, v, w;
}; 

inline Camera::Camera(const Vec3f& lookFrom, const Vec3f& lookAt, const Vec3f& upVector, float fov, float aspectRatio, float aperture, float focusDistance)
{
	m_Origin = lookFrom;
	m_LensRadius = aperture / 2;

	w = (lookFrom - lookAt).Normalize();
	u = upVector.cross(w).Normalize();
	v = w.cross(u);

	// fov in rad
	float theta = fov * 3.14159f / 180;
	float halfHeight = tan(theta / 2);
	float halfWidth = aspectRatio * halfHeight;

	//m_BottomLeftCorner = Vec3f(-halfWidth, -halfHeight, -1);
	m_BottomLeftCorner = m_Origin - u * halfWidth * focusDistance - v * halfHeight * focusDistance - w * focusDistance;

	m_HorizontalStep = u * 2 * halfWidth * focusDistance;
	m_VerticalStep = v * 2 * halfHeight * focusDistance;

}

inline Ray Camera::GetRay(float s, float t) const
{
	// create a ray using camera as starting point and calculated vector as direction
	Vec3f rd = RandomPointInUnitDisk() * m_LensRadius;
	Vec3f offset = u * rd.x + v * rd.y;
	return Ray(m_Origin + offset, m_BottomLeftCorner + m_HorizontalStep * s + m_VerticalStep * t - m_Origin - offset);
}
