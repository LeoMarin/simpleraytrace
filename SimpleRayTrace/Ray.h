#pragma once

#include "Vector.h"

class Material;

struct HitRecord
{
	Vec3f hitPoint;
	Vec3f normal;
	Vec3f albedo;
	float tMin;
	float tMax;
	Material* material;
};

class Ray
{
public:
	// constructor
	Ray(const Vec3f& origin, const Vec3f& direction)
		: m_Origin(origin), 
		m_Direction(direction.Normalize()),
		m_HitRecord({ Vec3f(), Vec3f(), Vec3f(), 0.001f, (float)INT_MAX, nullptr})
	{
	}

	// destructor
	~Ray() {}

	// getter functions
	inline Vec3f Direction() { return m_Direction; }
	inline Vec3f Direction() const { return m_Direction; }
	inline Vec3f Origin() { return m_Origin; }
	inline Vec3f Origin() const { return m_Origin; }

	// gets point on ray p(t) = origin + t * direction
	Vec3f PointOnRay(float t) const { return m_Origin + t * m_Direction; }

	// reference to last hit
	HitRecord& CurrentHitRecord() { return m_HitRecord; }

	// getter functions for last hit
	float TMax() const { return m_HitRecord.tMax; }
	float TMin() const { return m_HitRecord.tMin; }
	Vec3f HitNormal() const { return m_HitRecord.normal; }
	Vec3f HitPoint() const { return m_HitRecord.hitPoint; }
	Vec3f HitAlbedo() const { return m_HitRecord.albedo; }
	Material* HitMaterial() const { return m_HitRecord.material; }

	// setter functions
	void SetTMax(float tMax) { m_HitRecord.tMax = tMax; }
	void SetHitPoint(const Vec3f& point) { m_HitRecord.hitPoint = point; }
	void SetNormalInHitPoint(const Vec3f& normal) { m_HitRecord.normal = normal; }
	void SetHitAlbedo(const Vec3f& albedo) { m_HitRecord.albedo = albedo; }
	void SetMaterialPointer(Material* material) { m_HitRecord.material = material; }

private:
	// origin of ray
	Vec3f m_Origin;
	// direction of ray, normal vector
	Vec3f m_Direction;
	// last hit object
	HitRecord m_HitRecord;
};