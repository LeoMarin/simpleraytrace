#pragma once
#include <cmath>

template <typename t>
struct Vector3
{
public:
	// default constructor
	Vector3();
	// explicit constructors
	Vector3(t value);
	Vector3(t x, t y, t z);
	// copy constructor
	template<typename u>
	Vector3(const Vector3<u>& other);

	// destructor
	~Vector3() {}

	// returns length of vector as a float
	float Length() const;
	// returns squard length of vector as a float
	float LengthSquared() const;
	// returns vector of same type and direction, but of length 1
	Vector3<float> Normalize() const;

	// dot product of two vectors
	template<typename u>
	float dot(const Vector3<u>& other) const;
	static float dot(const Vector3<t>& v1, const Vector3<t>& v2);

	// cross product of two vectors
	template<typename u>
	Vector3<t> cross(const Vector3<u>& other) const;
	static Vector3<t> cross(const Vector3<t>& v1, const Vector3<t>& v2);

	// returns reflection of vector from a normal
	static Vector3<t> Reflect(const Vector3<t>& vector, const Vector3<t>& normal);

	// subscript operator
	t& operator[](const int i);
	const t& operator[](const int i) const;

	// aritmetic operator overloading
	template<typename u>
	Vector3 operator+(const Vector3<u>& other) const;
	template<typename u>
	Vector3 operator-(const Vector3<u>& other) const;
	template<typename u>
	Vector3 operator*(const Vector3<u>& other) const;
	template<typename u>
	Vector3<float> operator/(const Vector3<u>& other) const;
	template<typename u>
	Vector3 operator+(const u other) const;
	template<typename u>
	Vector3 operator-(const u other) const;
	template<typename u>
	Vector3 operator*(const u other) const;
	template<typename u>
	Vector3<float> operator/(const u other) const;
	Vector3 operator-() const;

	// assignment operator overloading
	template<typename u>
	Vector3 operator=(const Vector3<u>& other);
	template<typename u>
	Vector3 operator+=(const Vector3<u>& other);
	template<typename u>
	Vector3 operator-=(const Vector3<u>& other);
	template<typename u>
	Vector3 operator*=(const Vector3<u>& other);
	template<typename u>
	Vector3 operator/=(const Vector3<u>& other);
	template<typename u>
	Vector3 operator+=(const u other) const;
	template<typename u>
	Vector3 operator-=(const u other) const;
	template<typename u>
	Vector3 operator*=(const u other);
	template<typename u>
	Vector3<float> operator/=(const u other);

	// comparison operator overloading
	template<typename u>
	bool operator==(const Vector3<u>& other) const;

	// insertion operator overloading
	friend std::ostream& operator<<(std::ostream& os, const Vector3& v)
	{
		// write vector as "x y z"
		os << v.x << " " << v.y << " " << v.z;
		return os;
	}

	t x, y, z;
};

template <typename t>
inline Vector3<t>::Vector3()
	:x(0), y(0), z(0)
{
	// default constructor
}

template<typename t>
inline Vector3<t>::Vector3(t value)
	:x(value), y(value), z(value)
{
	// explicit constructor
}

template <typename t>
inline Vector3<t>::Vector3(t x, t y, t z)
	:x(x), y(y), z(z)
{
	// explicit constructor
}

template <typename t> template<typename u>
inline Vector3<t>::Vector3(const Vector3<u>& other)
{
	// copy constructor
	// if casting to int add +0.5 to round correctly
	x = static_cast<t>(other.x + ((std::is_same<t, int>::value) ? 0.5f : 0));
	y = static_cast<t>(other.y + ((std::is_same<t, int>::value) ? 0.5f : 0));
	z = static_cast<t>(other.z + ((std::is_same<t, int>::value) ? 0.5f : 0));
}

template <typename t>
inline float Vector3<t>::Length() const
{
	// Pythagorean theorem for finding length of the vector
	// l = sqrt(x^2 + y^2 + z^2)
	return sqrt(LengthSquared());
}

template<typename t>
inline float Vector3<t>::LengthSquared() const
{
	// Pythagorean theorem for finding length of the vector
	// l = sqrt(x^2 + y^2 + z^2)
	// l^2 = x^2 + y^2 + z^2
	return x * x + y * y + z * z;
}

template <typename t>
inline Vector3<float> Vector3<t>::Normalize() const
{
	// returns normal vector
	// normal vector has the same direction, but is of length 1
	return (*this) / Length();
}

template <typename t> template<typename u>
inline float Vector3<t>::dot(const Vector3<u>& other) const
{
	// returns dot product of two vectors
	// a (dot) b = x1*x2 + y1*y2 + z1*z2
	return x * other.x + y * other.y + z * other.z;
}

template<typename t>
inline float Vector3<t>::dot(const Vector3<t>& v1, const Vector3<t>& v2)
{
	// returns dot product of two vectors
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

template<typename t> template<typename u>
inline Vector3<t> Vector3<t>::cross(const Vector3<u>& other) const
{
	// returns cross product of two vectors
	Vector3 temp;
	temp.x = static_cast<t>(y * other.z - z * other.y);
	temp.y = static_cast<t>(z * other.x - x * other.z);
	temp.z = static_cast<t>(x * other.y - y * other.x);
	return temp;
}


template<typename t>
inline Vector3<t> Vector3<t>::cross(const Vector3<t>& v1, const Vector3<t>& v2)
{
	// returns cross product of two vectors
	Vector3 temp;
	temp.x = static_cast<t>(v1.y * v2.z - v1.z * v2.y);
	temp.y = static_cast<t>(v1.z * v2.x - v1.x * v2.z);
	temp.z = static_cast<t>(v1.x * v2.y - v1.y * v2.x);
	return temp;
}

template<typename t>
inline Vector3<t> Vector3<t>::Reflect(const Vector3<t>& vector, const Vector3<t>& normal)
{
	// calculate reflection ray around given
	return vector - normal * 2 * vector.dot(normal);
}

template<typename t>
inline t& Vector3<t>::operator[](const int i)
{
	// subscript operator overloading
	return (i == 0) ? x : ((i == 1) ? y : z);
}

template<typename t>
inline const t& Vector3<t>::operator[](const int i) const
{
	// constant subscript operator overloading
	return (i == 0) ? x : ((i == 1) ? y : z);
}

template<typename t>
inline Vector3<t> Vector3<t>::operator-() const
{
	// returns vector in opposite direction with same length
	return Vector3<t>(-x, -y, -z);
}

template <typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator+(const Vector3<u>& other) const
{
	// returns sum of two vectors
	// a + b = Vector(x1+x2, y1+y2, z1+z2)
	return Vector3(x + other.x, y + other.y, z + other.z);
}

template <typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator-(const Vector3<u>& other) const
{
	// returns difference of two vectors
	// a - b = Vector(x1-x2, y1-y2, z1-z2)
	return Vector3(x - other.x, y - other.y, z - other.z);
}

template <typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator*(const Vector3<u>& other) const
{
	// returns product of two vectors
	// a * b = Vector(x1*x2, y1*y2, z1*z2)
	// strictly speaking, not a valid mathematical operation
	return Vector3(x * other.x, y * other.y, z * other.z);
}

template<typename t> template<typename u>
inline Vector3<float> Vector3<t>::operator/(const Vector3<u>& other) const
{
	// returns quotient of two vectors
	// a * b = Vector(x1/x2, y1/y2, z1/z2)
	// strictly speaking, not a valid mathematical operation
	return Vector3<float>((float)x / other.x, (float)y / other.y, (float)z / other.z);
}

template<typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator+(const u other) const
{
	// returns sum of a vector and a scalar
	// v + s = Vector(x+s, y+s, z+s)
	return Vector3(x + other, y + other, z + other);
}

template<typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator-(const u other) const
{
	// returns difference of a vector and a scalar
	// v - s = Vector(x-s, y-s, z-s)
	return Vector3(x - other, y - other, z - other);
}

template <typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator*(const u other) const
{
	// returns product of a vector and a scalar
	// v * s = Vector(x*s, y*s, z*s)
	return Vector3(x * other, y * other, z * other);
}

template <typename t> template<typename u>
inline Vector3<float> Vector3<t>::operator/(const u other) const
{
	// returns quotient of a vector and a scalar
	// v / s = Vector(x/s, y/s, z/s)
	// more comonly represented as v * 1/s
	return Vector3(x / other, y / other, z / other);
}

template <typename t> template<typename u>
Vector3<t> Vector3<t>::operator=(const Vector3<u>& other)
{
	// assignment operator overload
	// if casting to int add +0.5 to round correctly
	x = static_cast<t>(other.x + ((std::is_same<t, int>::value) ? 0.5f : 0));
	y = static_cast<t>(other.y + ((std::is_same<t, int>::value) ? 0.5f : 0));
	z = static_cast<t>(other.z + ((std::is_same<t, int>::value) ? 0.5f : 0));
	return (*this);
}

template<typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator+=(const Vector3<u>& other)
{
	// returns sum of two vectors
	// a + b = Vector(x1+x2, y1+y2, z1+z2)
	x += other.x;
	y += other.y;
	z += other.z;
	return (*this);
}

template<typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator-=(const Vector3<u>& other)
{
	// returns difference of two vectors
	// a - b = Vector(x1-x2, y1-y2, z1-z2)
	x -= other.x;
	y -= other.y;
	z -= other.z;
	return (*this);
}

template<typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator*=(const Vector3<u>& other)
{
	// returns product of two vectors
	// a * b = Vector(x1*x2, y1*y2, z1*z2)
	// strictly speaking, not a valid mathematical operation
	x *= other.x;
	y *= other.y;
	z *= other.z;
	return (*this);
}

template<typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator/=(const Vector3<u>& other)
{
	// returns quotient of two vectors
	// a * b = Vector(x1/x2, y1/y2, z1/z2)
	// strictly speaking, not a valid mathematical operation
	x /= other.x;
	y /= other.y;
	z /= other.z;
	return (*this);
}

template<typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator+=(const u other) const
{
	// returns sum of vector and a scalar
	// v + s = Vector(x+s, y+s, z+s)
	x += other.x;
	y += other.y;
	z += other.z;
	return (*this);
}

template<typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator-=(const u other) const
{
	// returns difference of vector and a scalar
	// v - s = Vector(x-s, y-s, z-s)
	x -= other.x;
	y -= other.y;
	z -= other.z;
	return (*this);
}

template<typename t> template<typename u>
inline Vector3<t> Vector3<t>::operator*=(const u other)
{
	// returns product of vector and a scalar
	// v * s = Vector(x*s, y*s, z*s)
	x *= other.x;
	y *= other.y;
	z *= other.z;
	return (*this);
}

template<typename t> template<typename u>
inline Vector3<float> Vector3<t>::operator/=(const u other)
{
	// returns quotient of vector and a scalar
	// v / s = Vector(x/s, y/s, z/s)
	x /= other.x;
	y /= other.y;
	z /= other.z;
	return (*this);
}

template<typename t> template<typename u>
inline bool Vector3<t>::operator==(const Vector3<u>& other) const
{
	// comparison operator
	return x == other.x && y == other.y && z == other.z;
}

/*
*	scalar and vector operators
*/

template<typename t>
inline Vector3<t> operator+(const int scalar, const Vector3<t>& vector)
{
	// returns sum of a vector and a scalar
	// v + s = Vector(x+s, y+s, z+s)
	return Vector3<t>(vector.x + scalar, vector.y + scalar, vector.z + scalar);
}

template<typename t>
inline Vector3<t> operator+(const float scalar, const Vector3<t>& vector)
{
	// returns sum of a vector and a scalar
	// v + s = Vector(x+s, y+s, z+s)
	return Vector3<t>(vector.x + scalar, vector.y + scalar, vector.z + scalar);
}

template <typename t>
inline Vector3<t> operator*(const int scalar, const Vector3<t>& vector)
{
	// returns product of a vector and a scalar
	// v * s = Vector(x*s, y*s, z*s)
	return Vector3<t>(vector.x * scalar, vector.y * scalar, vector.z * scalar);
}


template <typename t>
inline Vector3<t> operator*(const float scalar, const Vector3<t>& vector)
{
	// returns product of a vector and a scalar
	// v * s = Vector(x*s, y*s, z*s)
	return Vector3<t>(vector.x * scalar, vector.y * scalar, vector.z * scalar);
}

typedef Vector3<float> Vec3f;
typedef Vector3<int> Vec3i;