#pragma once

#include <vector>
#include "Camera.h"
#include "Ray.h"
#include "Geometry.h"
#include "Random.h"

void GenerateRandomScene(std::vector<Sphere>& spheres)
{
	// generate scene from the book
	int n = 500;
	spheres.push_back(Sphere(Vec3f(0, -1000, 0), 1000, new Lambertian({ 0.5f, 0.5f, 0.5f })));

	for (int a = -11; a < 11; a++)
	{
		for (int b = -11; b < 11; b++)
		{
			float choose_mat = RandomFloat();
			Vec3f center(a + 0.9f * RandomFloat(), 0.2f, b + 0.9f * RandomFloat());
			if ((center - Vec3f(4, 0.2, 0)).Length() > 0.9)
			{
				if (choose_mat < 0.8f)
				{  // diffuse
					spheres.push_back(Sphere(
						center, 0.2f,
						new Lambertian(Vec3f(RandomFloat() * RandomFloat(),
							RandomFloat() * RandomFloat(),
							RandomFloat() * RandomFloat()))
					));
				}
				else if (choose_mat < 0.95f)
				{ // metal
					spheres.push_back(Sphere(
						center, 0.2f,
						new Metalic(Vec3f(RandomFloat(),
							RandomFloat(),
							RandomFloat()),
							RandomFloat())
					));
				}
				else
				{  // glass
					spheres.push_back(Sphere(center, 0.2f, new Dielectric({ 1,1,1 }, 1.5f)));
				}
			}
		}
	}

	spheres.push_back(Sphere(Vec3f(0, 1, 0), 1, new Dielectric({ 0.8f, 0.8f, 0.8f }, 1.5f)));
	spheres.push_back(Sphere(Vec3f(-4, 1, 0), 1, new Lambertian({ 0.4f, 0.2f, 0.1f })));
	spheres.push_back(Sphere(Vec3f(4, 1, 0), 1, new Metalic({ 0.7f, 0.6f, 0.5f }, 0)));
}

class Scene
{
public:
	Scene();
	
	// returns true if ray hits something
	bool Hit(Ray& ray) const;

	Vec3f LastHitPoint();
	Vec3f NormalInLastHitPoint();

	Ray GetRay(float u, float v) const;

private:
	std::vector<Sphere> m_Spheres;
};

inline Scene::Scene()
{
	//GenerateRandomScene(m_Spheres);

	//m_Spheres.push_back(Sphere({ 0, 0, 0 }, 0.5f, new Lambertian({ 0.8f, 0.3f, 0.3f })));
	m_Spheres.push_back(Sphere({ 0, 0, 0 }, 0.5f, new Lambertian({ 0.6f, 0.4f, 0.4f })));
	m_Spheres.push_back(Sphere({ 0, -100.5, 0 }, 100, new Lambertian({ 0.7f, 0.7f, 0.7f })));
	//m_Spheres.push_back(Sphere({ 1, 0, -1 }, 0.5f, new Metalic({ 0.8f, 0.6f, 0.2f }, 0.3f)));
	//m_Spheres.push_back(Sphere({ -1, 0, -1 }, 0.5f, new Dielectric({ 1, 1, 1 }, 1.5f)));
}

inline bool Scene::Hit(Ray& ray) const
{
	// check if ray hits any of the marbles
	bool hitAnything = false;
	for (auto& sphere : m_Spheres)
	{
		if (sphere.Hit(ray))
			hitAnything = true;
	}
	return hitAnything;
}
