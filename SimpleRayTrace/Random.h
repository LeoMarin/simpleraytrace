#pragma once

#include <random>
#include "Vector.h"

inline float RandomFloat()
{
	return (rand() % 1000) / (float)1000;
}

inline Vec3f RandomPointInUnitSphere()
{
	Vec3f p;
	do
	{
		p = Vec3f(RandomFloat(), RandomFloat(), RandomFloat());
	} while (p.LengthSquared() >= 1);
	return p;
}

Vec3f RandomPointInUnitDisk()
{
	Vec3f point;
	do
	{
		point = Vec3f((rand() % 1000) / (float)1000, (rand() % 1000) / (float)1000, 0);
	} while (point.dot(point) >= 1);
	return point;
}