#include <iostream>
#include <vector>
#include <fstream>
#include <random>
#include <time.h>
#include <chrono>

#include "Vector.h"
#include "Ray.h"
#include "Geometry.h"
#include "Camera.h"
#include "Scene.h"

Vec3f Color(Ray&& ray, Scene& scene, int depth)
{
	// cast a ray and check if it hits anything
	if (scene.Hit(ray))
	{	
		Vec3f scatterDirection;
		if (depth < 50 && ray.HitMaterial()->Scatter(ray, scatterDirection))
		{
			Vec3f attenuation = ray.HitAlbedo();
			return attenuation * Color(Ray(ray.HitPoint(), scatterDirection), scene, depth + 1);
		}
		else
			return Vec3f();
	}
	// if ray does not hit the sphere draw background
	else
	{
		// get value of y and clamp it to 0 < t < 1
		float t = 0.5f * (ray.Direction().y + 1.f);
		// calculate color using linear interpolation (lerp)
		// blendedValue = (1-t)*startValue + t*endValue
		Vec3f color = Vec3f(1, 1, 1) * (1 - t) + Vec3f(0.5f, 0.7f, 1.0f) * t;
		// scale RGB values to 0 - 255
		return color;
	}
}

void Render(Scene& scene, const Camera& camera)
{
	const size_t width = 360 * 16.f / 9;
	const size_t height = 360;

	// sampling for antialiasing
	const size_t numberOfSamples = 100;
	//float offset[9] = { 0.15f, 0.50f, 0.85f };

	// open .ppm file
	std::ofstream ofs;
	ofs.open("./images/out.ppm");
	ofs << "P3\n" << width << " " << height << "\n255\n";

	// cast ray for every pixel final image
	for (size_t j = height; j > 0; j--)
	{
		for (size_t i = 0; i < width; i++)
		{
			Vec3f color;

			// cast multiple rays for single pixel
			for (size_t s = 0; s < numberOfSamples; s++)
			{
				// calculate parameters for current ray
				float offsetX = (rand() % 1000) / (float)1000;
				float u = (float)(i + offsetX) / width;
				
				float offsetY = (rand() % 1000) / (float)1000;
				float v = (float)(j - 1 + offsetY) / height;

				// get color of current pixel using ray tracing
				color += Color(camera.GetRay(u, v), scene, 0);
			}

			// get average color of current pixel
			color = color / numberOfSamples;

			// gama corelation
			//color = Vec3f(sqrt(color.x), sqrt(color.y), sqrt(color.z));

			// scale color to 0-255 for rgb values
			Vec3i rgbColor = color * 255;

			// write color to output file
			ofs << rgbColor << "\n";
		}
	}
	ofs.close();
}

int main()
{
	srand(time(NULL));
	auto start = std::chrono::high_resolution_clock::now();

	Vec3f lookFrom(13, 2, 3);
	Vec3f lookAt(0, 0, 0);
	float distanceToFocus = (lookAt - lookFrom).Length();
	float aperture = 0.1f;
	Camera camera = Camera(lookFrom, lookAt, Vec3f(0, 1, 0), 20, 16.f / 9, aperture, distanceToFocus);

	Scene scene;
	Render(scene, camera);
	
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<float> duration = end - start;
	std::cout << duration.count() << std::endl;

}