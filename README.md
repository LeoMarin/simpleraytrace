## Renderer using RayTracing from scratch

Renderer created using Ray Tracing with implemented global illumination, reflections and refractions. 
This is my implementation of "Ray Tracing in One Weekend" by Peter Shirley.

Current state of the project:

![current state](SimpleRayTrace/images/final.png)

[[_TOC_]]

### Step 0: Writing image to file

For start files are writen in simple .ppm format.\
.ppm files look like:

```
P3		// P3 means colors are in ASCII
255 255		// number of rows and columns
255		// max color value
0 0 51		// color for every pixel in RGB format
0 1 51
0 2 51
0 3 51
...
```

![writing image](SimpleRayTrace/images/writing_image.png)

### Step 1: Defining Ray

Ray consists of 2 vectors. Frist vector represents location of ray's starting point. 
The other vector represents direction the ray is pointing at.\
Direction is stored as a normal, while starting point is vector with length.

### Step 2: First render using Ray Tracing

Using Ray Tracing get color of each pixel. Color is determined by y value of direction vector.
Image was creted using linear interpolation (lerp) between light blue and white.\
Formula for lerp looks like:\
`blendedValue = (1-t)*startValue + t*endValue`\
Where t is y clamped to 0 < t < 1.

![first render](SimpleRayTrace/images/first_render.png)

### Step 3: Drawing a sphere to the screen

Using vector geometry calculate if sphere is intersected by the ray. 
If intersection exists color pixel in red, otherwise use lerp value.
Sphere is drawn without any shadows and illumination so it looks like a solid circle.

![first sphere](SimpleRayTrace/images/first_sphere.png)

### Step 4: Surface normals

After finding a point where the ray intersects the sphere, we can find 
surface normal for that point. If we use normal vectors and simply 
scale value to 0-255 we can use these values to get the following image.

![surface normals](SimpleRayTrace/images/surface_normals.png)

### Step 5: Multiple objects

To add multiple objects to scene we simply calculate distance from camera to 
every object for every ray trace. We only render the object with the lowest distance 
from the camera.

![multiple objects](SimpleRayTrace/images/multiple_objects.png)

### Step 6: Antialiasing

To deal with the rought edges we use multiplay rays per pixel and calculate 
the avarage value. This is most noticaple on edge pixels, which become mix 
of foreground and background.

![antialiasing](SimpleRayTrace/images/antialiasing.png)

### Step 7: Diffuse material

Diffuse materials do not emit light but scatter ambient light randomly of their surface.

Diffuse material with 20% reflections:\
![diffuse 20](SimpleRayTrace/images/diffuse20.png)

Diffuse material with 50% reflections:\
![diffuse 50](SimpleRayTrace/images/diffuse50.png)

Diffuse material with 80% reflections:\
![diffuse 80](SimpleRayTrace/images/diffuse80.png)

### Step 8: Metalic material

Metalic materials reflect the scene. Fuzzines parametar determines if reflected image will 
be more or less clear. 

![metalic](SimpleRayTrace/images/metalic.png)

### Step 9: Refractions

Using Snell's law we can calculate refracted ray. Using Schlick approximation we can 
determine if ray is refracted or reflected.

![refraction](SimpleRayTrace/images/refraction.png)

Using multiple refractive surfaces we can create a bubble:\
![bubble](SimpleRayTrace/images/bubble.png)


### Step 10: Moving the camera

![moving the camera](SimpleRayTrace/images/camera.png)

### Step 11: Defocus blur

To get defocus blur we need to simulate apature of the camera.

![defocus blur](SimpleRayTrace/images/defocus_blur.png)